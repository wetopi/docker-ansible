# About this Repo

Image with ansible used as executor during gitlab deploy


## Build image via docker hub (deprecated)

```bash
git tag 20.10.22-02
git push bitb master && git push bitb --tags
```

## Build image witrh wetopi gitlab

```bash
git push origin master

# if build is ok, then:
git tag 20.10.22-02
git push origin --tags
````

## Remove tag

```bash
TAG=20.10.22-02 &&  git tag --delete ${TAG} && git push origin --delete ${TAG}
```