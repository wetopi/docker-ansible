FROM ubuntu:20.04

RUN apt update \
    && apt install -y \
    git ansible wget curl \
    && rm -rf /var/lib/apt/list/*
